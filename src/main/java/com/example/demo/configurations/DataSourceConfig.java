package com.example.demo.configurations;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import javax.sql.DataSource;

@Configuration
public class DataSourceConfig {

    @Bean("datasource")
    @Profile("production")
    public DataSource dataSource() {

        DriverManagerDataSource managerDataSource = new DriverManagerDataSource();
        managerDataSource.setDriverClassName("org.postgresql.Driver");
        managerDataSource.setUrl("jdbc:postgresql://localhost:5432/mytest");
        managerDataSource.setUsername("postgres");
        managerDataSource.setPassword("Rupp2019");
        return managerDataSource;
    }

    @Bean
    @Profile("development")
    public DataSource development(){
        EmbeddedDatabaseBuilder builder = new EmbeddedDatabaseBuilder();
                builder.setType(EmbeddedDatabaseType.H2);
                builder.addScript("sql/table.sql");
                return builder.build();
    }






}
