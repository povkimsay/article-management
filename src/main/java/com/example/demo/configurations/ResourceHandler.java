package com.example.demo.configurations;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.validation.Valid;

@Configuration
public class ResourceHandler implements WebMvcConfigurer {


        @Value("${file.name.path}")
        String path;

        @Value("${file.image.client}")
        String client;


        @Override
        public void addResourceHandlers(ResourceHandlerRegistry registry) {

            registry.addResourceHandler(path+"**")
//                    .addResourceLocations("file:/"+client);
                    .addResourceLocations("file:"+client);
        }
}
