package com.example.demo.controllers;

import com.example.demo.models.Article;
import com.example.demo.services.ArticleService;
import com.example.demo.services.CategoryService;
import com.example.demo.services.impl.ArticleServiceImpl;
import com.example.demo.utilities.Paginate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Controller
@RequestMapping("/articles")
public class ArticleController {

    ArticleService articleService;
    CategoryService categoryService;

    @Autowired
    public void setArticleService(ArticleService articleService) {
        this.articleService = articleService;
    }

    @Autowired
    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }


    @GetMapping("/one/{id}")
    public String getOneArticle(@PathVariable("id") int id, ModelMap modelMap){
        Article article = this.articleService.getOne(id);
        System.out.println(article);
        modelMap.addAttribute("article",article);
        return "/articles/view";
    }


    @GetMapping("/delete/{id}")
    public String deleteArticle(@PathVariable("id") int id){
        System.out.println(id);
       this.articleService.deleteArticle(id);
        return "redirect:/index";
    }

    @GetMapping("/update/{id}")
    public String updateForm(@PathVariable("id")Integer id,ModelMap modelMap){
        Article article = this.articleService.getOne(id);
        modelMap.addAttribute("article",article);
        modelMap.addAttribute("categoryList",this.categoryService.getAll());
        return "/articles/update";
    }

    @PostMapping("/update/submit")
    public String update(@RequestParam("file")MultipartFile file,@ModelAttribute Article article,ModelMap modelMap){
        System.out.println(article);

        if(!file.isEmpty()){

            String path = "src/main/resources/fileImages";
            String filename = file.getOriginalFilename();

            String extension = filename.substring(filename.lastIndexOf("."));
            filename = UUID.randomUUID()+extension;


            try {
                Files.copy(file.getInputStream(), Paths.get(path,filename));
                System.out.println(filename);
            } catch (IOException e) {
                e.printStackTrace();
            }
            article.setImage(filename);
        }


        this.articleService.update(article);

        return "redirect:/index";
    }



    @GetMapping("/add")
    public String addingForm(ModelMap modelMap) {
        modelMap.addAttribute("article",new Article());
        modelMap.addAttribute("categoryList",this.categoryService.getAll());
        return "/articles/add";
    }


    @PostMapping("/save")
    public String save(@ModelAttribute Article article,@RequestParam("file")MultipartFile file){


        if(!file.isEmpty()){

            String path = "src/main/resources/fileImages";
            String filename = file.getOriginalFilename();
            String extension = filename.substring(filename.lastIndexOf("."));
            filename = UUID.randomUUID()+extension;
            try {
                Files.copy(file.getInputStream(), Paths.get(path,filename));
            } catch (IOException e) {
                e.printStackTrace();
            }
            article.setImage(filename);
            article.setCategory(article.getCategory());
        }

        this.articleService.insert(article);
        return "redirect:/index";
    }
}
