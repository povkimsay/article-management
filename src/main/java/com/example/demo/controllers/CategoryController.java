package com.example.demo.controllers;

import com.example.demo.models.Category;
import com.example.demo.services.ArticleService;
import com.example.demo.services.CategoryService;
import com.example.demo.utilities.Paginate;
import org.apache.ibatis.annotations.Delete;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/categories")
public class CategoryController {


    CategoryService categoryService;

    @Autowired
    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping("/all")
    public String getAllCategories(ModelMap modelMap, Paginate paginate) {
        int totalRecored=this.categoryService.count();
        paginate.setTotalCount(totalRecored);
        List<Category> categoryList = this.categoryService.getAllCategory(paginate);
        modelMap.addAttribute("categoryList", categoryList);
        modelMap.addAttribute("paginate",paginate);
        return "/categories/all-category";
    }



    @GetMapping("/add")
    public String addCategory(Model model){
        model.addAttribute("category",new Category());
        return "/categories/add-category";
    }


    @PostMapping("/add/submit")
    public String save(@ModelAttribute Category category){
        this.categoryService.addCategory(category);
        return "redirect:/categories/all";
    }



    @GetMapping("/update/{id}")
    public String update(@PathVariable("id")Integer id,ModelMap modelMap){


        Category category = this.categoryService.getOne(id);
        modelMap.addAttribute("category",category);
        return "/categories/update-category";
    }

    @PostMapping("/update/submit")
    public String updateSubmit(@ModelAttribute Category category){
        System.out.println(category);
        this.categoryService.update(category);
        return "redirect:/categories/all";
    }


    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") Integer id){
        this.categoryService.delete(id);
        return "redirect:/categories/all";
    }


}
