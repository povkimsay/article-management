package com.example.demo.controllers;

import com.example.demo.models.Article;
import com.example.demo.models.Filter;
import com.example.demo.services.ArticleService;
import com.example.demo.services.CategoryService;
import com.example.demo.utilities.Paginate;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class MainController {

    ArticleService articleService;
    CategoryService categoryService;

    @Autowired
    public void setArticleService(ArticleService articleService) {
        this.articleService = articleService;
    }

    @Autowired
    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping(value = {"/index","/","/articles/all"} )
    public String index(ModelMap modelMap, Paginate paginate,
                        @Param("filter") Filter filter){
        int totalRecord = this.articleService.countFilter(filter);
        paginate.setTotalCount(totalRecord);
        List<Article> articleList = this.articleService.getAllArticleFilter(paginate,filter);
        modelMap.addAttribute("paginate",paginate);
        modelMap.addAttribute("filter",filter);
        modelMap.addAttribute("articleList",articleList);
        modelMap.addAttribute("categoryList",this.categoryService.getAll());
        return "/index";
    }




}
