package com.example.demo.services;

import com.example.demo.models.Article;
import com.example.demo.models.Filter;
import com.example.demo.utilities.Paginate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ArticleService {

    public List<Article> getAllArticlePaginate(Paginate paginate);
    public List<Article> getAllArticleFilter(Paginate paginate, Filter filter);
    public int count();
    public int countFilter(Filter filter);
    public void deleteArticle(int id);
    public Article getOne(int id);
    public void insert(Article article);
    public void update(Article article);
    public boolean updateApi(Article article);







}
