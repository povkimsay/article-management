package com.example.demo.services;

import com.example.demo.models.Category;
import com.example.demo.utilities.Paginate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CategoryService {

    public List<Category> getAllCategory(Paginate paginate);
    public List<Category> getAll();

    public int count();
    public void addCategory(Category category);
    public Category getOne(int id);
    public void update(Category category);
    public void delete(int id);

}
