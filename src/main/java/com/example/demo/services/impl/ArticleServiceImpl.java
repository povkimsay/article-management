package com.example.demo.services.impl;

import com.example.demo.models.Article;
import com.example.demo.models.Filter;
import com.example.demo.repositories.ArticleRepository;
import com.example.demo.services.ArticleService;
import com.example.demo.utilities.Paginate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ArticleServiceImpl implements ArticleService {

    private ArticleRepository articleRepository;

    @Autowired
    public void setArticleRepository(ArticleRepository articleRepository) {
        this.articleRepository = articleRepository;
    }

    @Override
    public List<Article> getAllArticlePaginate(Paginate paginate) {
        return this.articleRepository.getAllArticlePaginate(paginate);
    }


    @Override
    public List<Article> getAllArticleFilter(Paginate paginate, Filter filter) {
        return this.articleRepository.getAllArticleFilter(paginate,filter);
    }

    @Override
    public int count() {
        return this.articleRepository.count();
    }


    @Override
    public int countFilter(Filter filter) {
        return this.articleRepository.countFilter(filter);
    }

    @Override
    public void deleteArticle(int id) {
        this.articleRepository.deleteArticle(id);
    }

    @Override
    public Article getOne(int id) {
        return this.articleRepository.getOne(id);
    }


    @Override
    public void insert(Article article) {
        System.out.println(article);
        this.articleRepository.insert(article);
    }


    @Override
    public void update( Article article) {
        this.articleRepository.update(article);
    }


    @Override
    public boolean updateApi(Article article) {
        return this.articleRepository.updateApi(article);
    }
}
