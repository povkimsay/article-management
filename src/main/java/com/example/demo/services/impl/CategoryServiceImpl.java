package com.example.demo.services.impl;

import com.example.demo.models.Category;
import com.example.demo.repositories.CategoryRepository;
import com.example.demo.services.CategoryService;
import com.example.demo.utilities.Paginate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    CategoryRepository categoryRepository;

    @Override
    public List<Category> getAllCategory(Paginate paginate) {
        return this.categoryRepository.getAllCategory(paginate);
    }


    @Override
    public List<Category> getAll() {
        return this.categoryRepository.getAll();
    }

    @Override
    public int count() {
        return this.categoryRepository.count();
    }

    @Override
    public void addCategory(Category category) {
        this.categoryRepository.addCategory(category);
    }

    @Override
    public Category getOne(int id) {
        return this.categoryRepository.getOne(id);
    }


    @Override
    public void update(Category category) {
        this.categoryRepository.update(category);
    }

    @Override
    public void delete(int id) {
        this.categoryRepository.delete(id);
    }
}
