package com.example.demo.restcontrollers;

import com.example.demo.models.Article;
import com.example.demo.models.Filter;
import com.example.demo.services.ArticleService;
import com.example.demo.utilities.Paginate;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("api/v1/articles")
public class ArticleRestController {

    ArticleService articleService ;

    public ArticleRestController(ArticleService articleService) {
        this.articleService = articleService;
    }

    @GetMapping("")
    public RequestEntity<Object> articles(){
//
//        List<Article> articles = this.articleService.getAllArticlePaginate(new Paginate(),new Filter());
//        Map<String,Object> map = new HashMap<>();
//        if(articles.isEmpty()){
//            map.put("status",false);
//            map.put("message","");
//        }else {
//            return new RequestEntity<Object>(map, HttpStatus.OK);
//        }
     return null;
    }


    @PostMapping("")
    public void insert(@RequestBody Article article){
        this.articleService.insert(article);
    }


    @PutMapping("")
    public boolean update(@RequestBody Article article){
        return this.articleService.updateApi(article);
    }



    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Integer id){
        this.articleService.deleteArticle(id);
    }






}
