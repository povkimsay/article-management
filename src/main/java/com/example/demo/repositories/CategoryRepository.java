package com.example.demo.repositories;

import com.example.demo.models.Article;
import com.example.demo.models.Category;
import com.example.demo.repositories.providers.CategoryProvider;
import com.example.demo.utilities.Paginate;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;

@Repository
public interface CategoryRepository {


    @SelectProvider(type = CategoryProvider.class,method = "getAllCategory")
    public List<Category> getAllCategory(Paginate paginate);


    @Select("select *from ams_category where status is true order by id")
    public List<Category> getAll();




    @Select("Select count(*) from ams_category")
    public int count();



    @Insert("Insert into ams_category(name) VALUES(#{name}) ")
    @Options(useGeneratedKeys = true)
    public void addCategory(Category category);


    @Select("select * from ams_category where id=#{id}")
    public Category getOne(int id);


    @Update("Update ams_category set name=#{name} where id=#{id}")
    @Options(useGeneratedKeys = true)
    public void update(Category category);


    @Update("update ams_category set status = false where id=#{id};" +
            " update ams_article set status = false where cate_id=#{id}")
    public void delete(int id);

}
