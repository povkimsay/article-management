package com.example.demo.repositories.providers;


import com.example.demo.models.Category;
import com.example.demo.utilities.Paginate;
import org.apache.ibatis.jdbc.SQL;

public class CategoryProvider {

    public String getAllCategory(Paginate paginate){
        return new SQL(){{
            SELECT("*");
            FROM("ams_category");
            WHERE("status is true");
            ORDER_BY("id LIMIT #{limit} OFFSET #{offset}");
        }}.toString();
    }



}
