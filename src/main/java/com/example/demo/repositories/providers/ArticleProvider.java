package com.example.demo.repositories.providers;

import com.example.demo.models.Filter;
import com.example.demo.utilities.Paginate;
import org.apache.ibatis.jdbc.SQL;

public class ArticleProvider {



    public String getAll(Paginate paginate, Filter filter) {

        System.out.println(filter.getTitle());
        System.out.println(filter.getCate_id());

        return new SQL(){{
            SELECT("*");
            FROM("ams_article a ");
            WHERE("status is true");
            if(filter.getTitle() !=null){
                WHERE("a.title ILIKE '%' ||  #{filter.title} || '%' ");
            }
            if(filter.getCate_id() !=null){
                WHERE("a.cate_id=#{filter.cate_id}");
            }
            ORDER_BY("a.id desc LIMIT #{paginate.limit} OFFSET #{paginate.offset}");

        }}.toString();
    }




    public String countFilter(Filter filter){

        return  new SQL(){{
            SELECT("COUNT(id)");
            FROM("ams_article");
            WHERE("status is true");
            if(filter.getTitle() !=null){
                WHERE("title ILIKE '%' ||  #{title} || '%' ");
            }
            if(filter.getCate_id() !=null){
                WHERE("cate_id=#{cate_id}");
            }

        }}.toString();
    }
}