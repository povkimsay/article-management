package com.example.demo.repositories;

import com.example.demo.models.Article;
import com.example.demo.models.Filter;
import com.example.demo.models.Category;
import com.example.demo.repositories.providers.ArticleProvider;
import com.example.demo.utilities.Paginate;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ArticleRepository {


//    @SelectProvider(type = ArticleProvider.class, method = "getAllArticleProvider")
    @Select("Select * from ams_article where status is true order by id desc " +
            " LIMIT #{limit} OFFSET #{offset}")
    @Results({
            @Result(property = "createdDate",column = "created_date"),
            @Result(property="category",column = "cate_id", one = @One(select ="getOneCate"))
    })
    @Options(useGeneratedKeys = true)
    public List<Article> getAllArticlePaginate(Paginate paginate);

    @Select("select * from ams_category where id=#{id}")
    public Category getOneCate(int id);








    @SelectProvider(type = ArticleProvider.class,method = "getAll")
    @Results({
            @Result(property = "createdDate",column = "created_date"),
            @Result(property="category",column = "cate_id", one = @One(select ="getOneCate"))
    })
    @Options(useGeneratedKeys = true)
    public List<Article> getAllArticleFilter(Paginate paginate, Filter filter);







    @Select("Select count(*) from ams_article where status is true")
    public int count();


    @SelectProvider(type = ArticleProvider.class,method = "countFilter")
    public int countFilter(Filter filter);




    @Select("Select * from ams_article where id=#{id}")
    public Article getOne(int id);


    @Delete("update ams_article set status = false where id=#{id}")
    public void deleteArticle(int id);



    @Insert("INSERT INTO ams_article(title,description,author,cate_id,image) " +
            "VALUES (#{title},#{description},#{author},#{category.id},#{image})")
    @Options(useGeneratedKeys = true)
    public void insert(Article article);




    @Update("update ams_article set title=#{title},description=#{description},author=#{author}," +
            " cate_id=#{category.id},image=#{image}" +
            " where id =#{id} ")
    @Options(useGeneratedKeys = true)
    public void update(Article article);


    @Update("update ams_article set title=#{title},description=#{description},author=#{author}," +
            " cate_id=#{category.id},image=#{image}" +
            " where id =#{id} ")
    @Options(useGeneratedKeys = true)
    public boolean updateApi(Article article);


}
