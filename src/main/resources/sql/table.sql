
CREATE TABLE ams_category(
  id INT PRIMARY KEY auto_increment,
  name VARCHAR(30),
  status BOOLEAN DEFAULT TRUE
);


INSERT  INTO ams_category(name) VALUES ('Entertainment'),
                                       ('Technology'),
                                       ('Resort'),
                                       ('Sport'),
                                       ('Health');


CREATE TABLE ams_article(
  id INT PRIMARY KEY auto_increment,
  title VARCHAR(50),
  description VARCHAR,
  author VARCHAR(30),
  cate_id INT REFERENCES ams_category(id),
  image VARCHAR,
  created_date timestamp default now(),
  status BOOLEAN DEFAULT TRUE
);

INSERT INTO ams_article (title, description, author,cate_id, image)
                VALUES  ('sitle 01','desc 1','tra','1','ee485393-9449-4802-a7eb-ce20014a1c7b.png'),
                        ('Title 02','desc 2','dara','3','ee485393-9449-4802-a7eb-ce20014a1c7b.png'),
                        ('Title 03','desc 3','sok','4','f44e08f9-6202-4424-89ce-24e351166503.png'),
                        ('tle 04','desc 4','dara','4','f44e08f9-6202-4424-89ce-24e351166503.png'),
                        ('Title 05','desc 5','roth','2','c1366d95-e9d0-48f5-9a85-86f2385d7eaf.PNG'),
                        ('atle 06','desc 6','dara','5','c1366d95-e9d0-48f5-9a85-86f2385d7eaf.PNG'),
                        ('bitle 07','desc 7','ty','2','ee485393-9449-4802-a7eb-ce20014a1c7b.png'),
                        ('citle 08','desc 8','ra','5','43fbff09-c8c7-4c2f-918c-ec09c8abdf50.png'),
                        ('ritle 09','desc 9','da','1','ee485393-9449-4802-a7eb-ce20014a1c7b.png'),
                        ('qitle 10','desc 10','sok','4','f44e08f9-6202-4424-89ce-24e351166503.png'),
                        ('uitle 11','desc 11','nara','3','ee485393-9449-4802-a7eb-ce20014a1c7b.png');


